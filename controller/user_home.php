<?php session_start(); ?>

<!DOCTYPE html>

<?php 
  require('../model/database.php');
  require ('../model/login_db.php');
  
  $username = filter_input(INPUT_POST, 'username');
  $password = filter_input(INPUT_POST, 'password');
  
  if($username != '' && $password != '')
    {
      $_SESSION['username'] = $username;
      $_SESSION['password'] = $password;
    }
  
  $result = getLoginInfo($_SESSION['username'], $_SESSION['password']);
   
  if($result === FALSE)
    {
      header('Location: http://localhost/Roomo/view/login_error.php');
    }
    
  $_SESSION['f_name'] = $result['CARRIER_FNAME'];
  $_SESSION['l_name'] = $result['CARRIER_LNAME'];         
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title> Homepage </title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href = "../view/css/user_home.css" rel = "stylesheet">
        <script type="text/javascript" src="../view/js/logOut.js"> </script>
    </head>
    
    <body>
      <nav>
        <label> <?php echo $_SESSION['f_name'].' '.$_SESSION['l_name']; ?> </label>
        <button type="button" onclick="logOut();" class="btn btn-default"> Log Out </button>
      </nav>
            
      <div class="all">
        <header> <img src="../images/logo.png" alt="roomo logo"> </header>
        
        <br>
        <br>
        
        <div>
            <p> <a href = "../view/search.php" > Search Equipment </a> <br/> </p>
            <p> <a href = "../view/move.php"> Move Equipment </a> <br/> </p>
        </div>
        
      </div>
        
    </body>
</html>

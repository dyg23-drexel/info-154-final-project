-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2017 at 04:09 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roomo`
--

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `BLDG_ID` int(1) NOT NULL,
  `BLDG_NAME` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`BLDG_ID`, `BLDG_NAME`) VALUES
(1, 'Laurel Hall Building'),
(2, 'Technology Building'),
(3, 'Science Building'),
(4, 'Enterprise Center'),
(5, 'Briggs Road Center');

-- --------------------------------------------------------

--
-- Table structure for table `carrier`
--

CREATE TABLE `carrier` (
  `CARRIER_ID` int(7) NOT NULL,
  `CARRIER_LNAME` varchar(9) DEFAULT NULL,
  `CARRIER_MI` varchar(1) DEFAULT NULL,
  `CARRIER_FNAME` varchar(8) DEFAULT NULL,
  `CARRIER_PNUM` int(10) DEFAULT NULL,
  `CARRIER_PASSWORD` varchar(8) NOT NULL DEFAULT 'hello'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carrier`
--

INSERT INTO `carrier` (`CARRIER_ID`, `CARRIER_LNAME`, `CARRIER_MI`, `CARRIER_FNAME`, `CARRIER_PNUM`, `CARRIER_PASSWORD`) VALUES
(1142584, 'Diop', '', 'Aissatou', NULL, 'hello'),
(1154547, 'Konte', '', 'Kardigue', NULL, 'hello'),
(1245865, 'Coulibaly', '', 'Hassan', NULL, 'hello'),
(1283138, 'Gbenou', 'Y', 'Didier', NULL, 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `EQ_ID` varchar(6) NOT NULL,
  `EQ_BRAND` varchar(27) DEFAULT NULL,
  `EQ_TYPE` varchar(15) DEFAULT NULL,
  `EQ_SN` varchar(15) DEFAULT NULL,
  `EQ_MOVE_DATE` varchar(10) DEFAULT NULL,
  `EQ_CARRIER` int(7) DEFAULT NULL,
  `EQ_LOCATION` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`EQ_ID`, `EQ_BRAND`, `EQ_TYPE`, `EQ_SN`, `EQ_MOVE_DATE`, `EQ_CARRIER`, `EQ_LOCATION`) VALUES
('C01   ', 'Dell Latitude E5510', 'Laptop', 'JDDMXM1', '2017-03-14', 1245865, 'LH209'),
('C02   ', 'DELL Latitude E5520', 'Laptop', 'FPBRLQ1', '2017-03-03', 1283138, 'TC202'),
('C03   ', 'DELL Latitude E5520', 'Laptop', '8803CT1', '', 1283138, 'EC222 '),
('C04   ', 'DELL Latitude E5520', 'Laptop', 'FPBXLQ1', '2017-03-14', 1283138, 'LH209'),
('C06   ', 'DELL Latitude E5520', 'Laptop', 'FPBTLQ1', '2017-03-14', 1283138, 'LH208'),
('C07   ', 'DELL Latitude E5520', 'Laptop', 'FPBSLQ1', '', 1283138, 'EC144 '),
('C08   ', 'DELL Latitude E5510', 'Laptop', '5MZT0N1', '2017-03-14', 1283138, 'LH304'),
('C09   ', 'DELL Latitude E5510', 'Laptop', '4MZT0N1', '2017-03-14', 1283138, 'TC208'),
('C10  ', 'DELL Latitude E5510', 'Laptop', '1MZTON1', '', 1283138, 'EC236 '),
('C16  ', 'DELL Latitude E5510', 'Laptop', '7lZTON1', '2017-03-14', 1283138, 'TC300'),
('C18  ', 'DELL Latitude E5510', 'Laptop', '4LZT0N1', '2017-03-14', 1283138, 'LH305'),
('C19  ', 'Dell Optiplex 980', 'Desktop', '8VSS8P1', '2017-03-14', 1283138, 'SB101'),
('C20  ', 'Dell Optiplex 980', 'Desktop', '8VST8P1', '', 1283138, 'LH116 '),
('D01 ', 'Elmo HV-5000XG', 'Document Camera', '506257', '2016-11-30', 1283138, 'LH116'),
('D02 ', 'Elmo EV-400AF', 'Document Camera', '208635', '', 1283138, 'BR103 '),
('D03 ', 'Elmo P10', 'Document Camera', '1008550', '', 1283138, 'LH206 '),
('D04 ', 'Elmo P10S', 'Document Camera', '960589', '', 1283138, 'LH134 '),
('D05 ', 'Elmo', 'Document Camera', '255857', '', 1283138, 'LH204 '),
('D06 ', 'Elmo', 'Document Camera', '255741', '', 1283138, 'LH2ND '),
('D07 ', 'Elmo P10S', 'Document Camera', '960579', '', 1283138, 'LH311 '),
('D08 ', 'Canon RE-350', 'Document Camera', '71103784', '', 1283138, 'BR103 '),
('D09 ', 'Elmo P10', 'Document Camera', '1008544', '', 1283138, 'BR110 '),
('D10', 'Elmo P10', 'Document Camera', '1008545', '', 1283138, 'LH134 '),
('D11', 'Elmo P10', 'Document Camera', '1008548', '', 1283138, 'TC202 '),
('D12', 'Elmo P10', 'Document Camera', '1008541', '', 1283138, 'BR122 '),
('D13', 'Samsung SVP-6000N', 'Document Camera', 'S1091255', '', 1283138, 'LH2ND '),
('D14', 'Samsung SVP-6000N', 'Document Camera', 'S1091289', '', 1283138, 'LH312 '),
('D15', 'Samsung SVP-6000N', 'Document Camera', 'S1091205', '2016-12-01', 1283138, 'SB101'),
('D16', 'Samsung SVP-6000N', 'Document Camera', 'S1091267', '', 1283138, 'SB115 '),
('DA ', 'Elmo', 'Document Camera', '960590', '', 1283138, 'TC112 '),
('DB ', 'Elmo', 'Document Camera', '960521', '', 1283138, 'LH132 '),
('DC', 'Elmo', 'Document Camera', '960520', '', 1283138, 'SB218 '),
('DD ', 'Elmo', 'Document Camera', '960560', '', 1283138, 'SB215 '),
('P01', 'Epson Powerlite 915W', 'Projector', 'PAKF250171L', '2017-03-14', 1154547, 'TC112'),
('P02', 'Epson Powerlite 915W', 'Projector', 'PAKF160456L', '', 1283138, 'EC245 '),
('P03 ', 'Epson Powerlite 915W', 'Projector', 'PAKF250164L', '', 1283138, 'EC222 '),
('P04', 'Epson Powerlite 915W', 'Projector', 'PAKF160326L', '', 1283138, 'EC144 '),
('P06', 'Epson Powerlite905', 'Projector', 'PAAF110179L', '', 1283138, 'LH134 '),
('P07', 'Sharp Notevision PG-F212X-L', 'Projector', '910925774', '', 1283138, 'EC144 '),
('P08', 'Sharp Notevision PG-F212X-L', 'Projector', '910925800', '', 1283138, 'SB101 '),
('P09', 'Sharp Notevision PG-F211X-L', 'Projector', '708914606', '', 1283138, 'LH203 '),
('P10   ', 'Sharp Notevision PG-F211X-L', 'Projector', '708914474', '2017-03-14', 1283138, 'TC215'),
('P16   ', 'Sharp Notevision PG-F211X-L', 'Projector', '708914079', '2017-03-14', 1283138, 'TC210'),
('P18   ', 'Sharp Notevision PG-F212X-L', 'Projector', '908923739', '', 1283138, 'BR105 '),
('P19   ', 'Vizio 55in', 'TV', 'LASPHLAL2700004', '', 1283138, 'LH2ND '),
('P20   ', 'Vizio 55in', 'TV', 'LAQPKIBM3201338', '', 1283138, 'LH116 ');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `ROOM_ID` varchar(6) NOT NULL,
  `ROOM_LOCATION` int(1) DEFAULT NULL,
  `ROOM_SIGNAL` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`ROOM_ID`, `ROOM_LOCATION`, `ROOM_SIGNAL`) VALUES
('BR101 ', 5, 'VGA  '),
('BR102 ', 5, 'VGA  '),
('BR103 ', 5, 'VGA  '),
('BR104 ', 5, 'VGA  '),
('BR105 ', 5, ''),
('BR109 ', 5, 'VGA  '),
('BR110 ', 5, 'VGA  '),
('BR111 ', 5, 'VGA  '),
('BR114 ', 5, ''),
('BR115 ', 5, ''),
('BR116 ', 5, 'VGA  '),
('BR118 ', 5, 'VGA  '),
('BR122 ', 5, 'VGA  '),
('BR123 ', 5, 'VGA  '),
('BRSTO ', 5, ''),
('EC130 ', 4, ''),
('EC131 ', 4, 'VGA  '),
('EC144 ', 4, ''),
('EC222 ', 4, ''),
('EC236 ', 4, ''),
('EC245 ', 4, ''),
('EC248 ', 4, 'DP   '),
('EC267 ', 4, 'HDMI '),
('LH116 ', 1, 'VGA  '),
('LH132 ', 1, 'HDMI '),
('LH134 ', 1, 'VGA  '),
('LH1ST ', 1, ''),
('LH203 ', 1, 'VGA  '),
('LH204 ', 1, 'VGA  '),
('LH205 ', 1, 'DP   '),
('LH206 ', 1, 'DP   '),
('LH207 ', 1, 'DP   '),
('LH208 ', 1, 'DP   '),
('LH209 ', 1, 'DP   '),
('LH210 ', 1, 'DP   '),
('LH211 ', 1, 'HDMI '),
('LH212 ', 1, 'HDMI '),
('LH2ND ', 1, ''),
('LH303 ', 1, 'DP   '),
('LH304 ', 1, 'DP   '),
('LH305 ', 1, 'DP   '),
('LH306 ', 1, 'DP   '),
('LH307 ', 1, 'DP   '),
('LH308 ', 1, 'HDMI '),
('LH309 ', 1, 'VGA  '),
('LH310 ', 1, 'DP   '),
('LH311 ', 1, 'VGA  '),
('LH312 ', 1, 'DP   '),
('LH320 ', 1, 'VGA  '),
('LH3RD ', 1, ''),
('SB100 ', 3, 'HDMI '),
('SB101 ', 3, 'HDMI '),
('SB115 ', 3, 'HDMI '),
('SB215 ', 3, 'DP   '),
('SB218 ', 3, 'DP   '),
('SB219 ', 3, 'DP   '),
('TC112 ', 2, 'VGA  '),
('TC200 ', 2, 'VGA  '),
('TC202 ', 2, 'DP   '),
('TC203 ', 2, 'DP   '),
('TC204 ', 2, 'DP   '),
('TC205 ', 2, ''),
('TC206 ', 2, 'DP   '),
('TC208 ', 2, 'HDMI '),
('TC209 ', 2, 'VGA  '),
('TC210 ', 2, ''),
('TC215 ', 2, 'VGA  '),
('TC300 ', 2, 'VGA  '),
('TC306 ', 2, ''),
('TC309 ', 2, ''),
('TC315 ', 2, 'DP   ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`BLDG_ID`);

--
-- Indexes for table `carrier`
--
ALTER TABLE `carrier`
  ADD PRIMARY KEY (`CARRIER_ID`),
  ADD UNIQUE KEY `CARRIER_PNUM` (`CARRIER_PNUM`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`EQ_ID`),
  ADD KEY `EQ_CARRIER` (`EQ_CARRIER`),
  ADD KEY `EQ_LOCATION` (`EQ_LOCATION`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`ROOM_ID`),
  ADD KEY `ROOM_LOCATION` (`ROOM_LOCATION`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipment`
--
ALTER TABLE `equipment`
  ADD CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`EQ_CARRIER`) REFERENCES `carrier` (`CARRIER_ID`),
  ADD CONSTRAINT `equipment_ibfk_2` FOREIGN KEY (`EQ_LOCATION`) REFERENCES `room` (`ROOM_ID`);

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`ROOM_LOCATION`) REFERENCES `building` (`BLDG_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

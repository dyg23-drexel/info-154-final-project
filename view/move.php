<?php session_start(); ?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title> Move </title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href = "../view/css/move.css" rel = "stylesheet">
        <script type="text/javascript">
            function getMoveResult()
  {
    var comp = document.getElementById('comp').value;
    var proj = document.getElementById('proj').value;
    var dcam = document.getElementById('dcam').value;
    
    var lh = document.getElementById('LH').value;
    var tec = document.getElementById('TEC').value;
    var sb = document.getElementById('SB').value;
    var ec = document.getElementById('EC').value;
    var br = document.getElementById('BR').value;
    
    var xhttp;
    
    if(window.XMLHttpRequest)
      { xhttp = new XMLHttpRequest(); }
      
    else
      { xhttp = new ActiveXObject('Microsoft.XMLHTTP'); }
      
    xhttp.open('POST', 'move_conf.php', false);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    
    var query = 'comp=' + comp
                + '&proj=' + proj
                + '&dcam=' + dcam
                + '&LH=' + lh
                + '&TEC=' + tec
                + '&SB=' + sb
                + '&EC=' + ec
                + '&BR=' + br;
    
    xhttp.send(query);
    
    alert(xhttp.responseText); 
     
  }
        </script>
        <script type="text/javascript" src="../view/js/logOut.js"> </script>
    </head>
    <body>
     <nav>
        <label> <?php echo $_SESSION['f_name'].' '.$_SESSION['l_name']; ?> </label>
        <button type="button" onclick="logOut();" class="btn btn-default"> Log Out </button>
     </nav>
     <p class="home"> <a href = "../controller/user_home.php"> Home </a> </p>
     <header> <img src="../images/logo.png" alt="roomo logo"> </header>
        
     <h1> Move Equipment </h1>
        
     <form method="post" action="move_conf.php" class="form-horizontal">
          
        <div id = "all">
            
            <div id = "equip" class="form-group">
             
          <h2> SELECT EQUIPMENT </h2>   
             
          <label> Computers </label>
            
           <select name = "comp" id = "comp" class="form-control">
                
                <option value = "blank"> Select </option>
                <option value = "C01"> C1 </option>
                <option value = "C02"> C2 </option>
                <option value = "C03"> C3 </option>
                <option value = "C04"> C4 </option>
                <option value = "C06"> C6 </option>
                <option value = "C07"> C7 </option>
                <option value = "C08"> C8 </option>
                <option value = "C09"> C9 </option>
                <option value = "C10"> C10 </option>
                <option value = "C16"> C16 </option>
                <option value = "C18"> C18 </option>
                <option value = "C19"> C19 </option>
                <option value = "C20"> C20 </option>
                
            </select>
            
          <label> Projectors </label>
            
            <select name = "proj" id = "proj" class="form-control">
                
                <option value = "blank"> Select </option>
                <option value = "P01"> P1 </option>
                <option value = "P02"> P2 </option>
                <option value = "P03"> P3 </option>
                <option value = "P04"> P4 </option>
                <option value = "P06"> P6 </option>
                <option value = "P07"> P7 </option>
                <option value = "P08"> P8 </option>
                <option value = "P09"> P9 </option>
                <option value = "P10"> P10 </option>
                <option value = "P16"> P16 </option>
                <option value = "P18"> P18 </option>
                <option value = "P19"> P19 </option>
                <option value = "P20"> P20 </option>
             
            </select>
            
          <label> Document Cameras </label>
            
            <select name = "dcam" id = "dcam" class="form-control">
                
                <option value = "blank"> Select </option>
                <option value = "D01"> DC 1 </option>
                <option value = "D02"> DC 2 </option>
                <option value = "D03"> DC 3 </option>
                <option value = "D04"> DC 4 </option>
                <option value = "D05"> DC 5 </option>
                <option value = "D06"> DC 6 </option>
                <option value = "D07"> DC 7 </option>
                <option value = "D08"> DC 8 </option>
                <option value = "D09"> DC 9 </option>
                <option value = "D10"> DC 10 </option>
                <option value = "D11"> DC 11 </option>
                <option value = "D12"> DC 12 </option>
                <option value = "D13"> DC 13 </option>
                <option value = "D14"> DC 14 </option>
                <option value = "D15"> DC 15 </option>
                <option value = "D16"> DC 16 </option>
                <option value = "DA"> DC A </option>
                <option value = "DB"> DC B </option>
                <option value = "DC"> DC C </option>
                <option value = "DD"> DC D </option>
            
            </select>
          
          </div>
            
          <div id = "room" class="form-group">  
           
           <h2> SELECT LOCATION </h2>
              
           <label> Laurel Hall Building </label>
          
              <select name = "LH" id = "LH" class="form-control">
                  
                <option value = "blank"> Select </option>
                <option value = "LH116"> LH 116 </option>
                <option value = "LH132"> LH 132 </option>
                <option value = "LH134"> LH 134 </option>
                <option value = "LH1ST"> LH 1ST </option>
                <option value = "LH203"> LH 203 </option>
                <option value = "LH204"> LH 204 </option>
                <option value = "LH205"> LH 205 </option>
                <option value = "LH206"> LH 206 </option>
                <option value = "LH207"> LH 207 </option>
                <option value = "LH208"> LH 208 </option>
                <option value = "LH209"> LH 209 </option>
                <option value = "LH210"> LH 210 </option>
                <option value = "LH211"> LH 211 </option>
                <option value = "LH212"> LH 212 </option>
                <option value = "LH2ND"> LH 2ND </option>
                <option value = "LH303"> LH 303 </option>
                <option value = "LH304"> LH 304 </option>
                <option value = "LH305"> LH 305 </option>
                <option value = "LH306"> LH 306 </option>
                <option value = "LH307"> LH 307 </option>
                <option value = "LH308"> LH 308 </option>
                <option value = "LH309"> LH 309 </option>
                <option value = "LH310"> LH 310 </option>
                <option value = "LH311"> LH 311 </option>
                <option value = "LH312"> LH 312 </option>
                <option value = "LH320"> LH 320 </option>
                <option value = "LH3RD"> LH 3RD </option>
                  
              </select>
              
           <label> TEC Building </label>
              
              <select name = "TEC" id = "TEC" class="form-control">
                  
                <option value = "blank"> Select </option>
                <option value = "TC112"> TC 112 </option>
                <option value = "TC200"> TC 200 </option>
                <option value = "TC202"> TC 202 </option>
                <option value = "TC203"> TC 203 </option>
                <option value = "TC204"> TC 204 </option>
                <option value = "TC205"> TC 205 </option>
                <option value = "TC206"> TC 206 </option>
                <option value = "TC208"> TC 208 </option>
                <option value = "TC209"> TC 209 </option>
                <option value = "TC210"> TC 210 </option>
                <option value = "TC215"> TC 215 </option>
                <option value = "TC300"> TC 300 </option>
                <option value = "TC306"> TC 306 </option>
                <option value = "TC309"> TC 309 </option>
                <option value = "TC315"> TC 315 </option>
                  
              </select>
            
           <label> Science Building </label>
              
              <select name = "SB" id = "SB" class="form-control">
                  
                <option value = "blank"> Select </option>
                <option value = "SB100"> SB 100 </option>
                <option value = "SB101"> SB 101 </option>
                <option value = "SB115"> SB 115 </option>
                <option value = "SB215"> SB 215 </option>
                <option value = "SB218"> SB 218 </option>
                <option value = "SB219"> SB 219 </option>
                  
              </select>
            
           <label> Enterprise Center </label>
              
              <select name = "EC" id = "EC" class="form-control">
                  
                <option value = "blank"> Select </option>
                <option value = "EC130"> EC 130 </option>
                <option value = "EC131"> EC 131 </option>
                <option value = "EC144"> EC 144 </option>
                <option value = "EC222"> EC 222 </option>
                <option value = "EC236"> EC 236 </option>
                <option value = "EC245"> EC 245 </option>
                <option value = "EC248"> EC 248 </option>
                <option value = "EC267"> EC 267 </option>
                  
              </select>
            
           <label> Briggs Center </label>
              
              <select name = "BR" id = "BR" class="form-control">
                  
                <option value = "blank"> Select </option>
                <option value = "BR101"> BR 101 </option>
                <option value = "BR102"> BR 102 </option>
                <option value = "BR103"> BR 103 </option>
                <option value = "BR104"> BR 104 </option>
                <option value = "BR105"> BR 105 </option>
                <option value = "BR109"> BR 109 </option>
                <option value = "BR110"> BR 110 </option>
                <option value = "BR111"> BR 111 </option>
                <option value = "BR114"> BR 114 </option>
                <option value = "BR115"> BR 115 </option>
                <option value = "BR116"> BR 116 </option>
                <option value = "BR118"> BR 118 </option>
                <option value = "BR122"> BR 122 </option>
                <option value = "BR123"> BR 123 </option>
                <option value = "BRSTO"> BR STO </option>
                  
              </select>
            
          </div>
          
         </div>
          
          <br>
            
          <button type = "button" class = "btn btn-default" onclick="getMoveResult();"> Move </button>
              
        </form>
      
    </body>
</html>
<!DOCTYPE html>

<html>
	<head>
		<meta charset = "utf-8"/>
		<title> Login </title>
                <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
                <link href="../view/css/index.css" rel="stylesheet">
	</head>
	
	<body>
            
          <div class="all">
              <header> <img src="../images/logo.png" alt="roomo logo"> </header>
            
              <form class="form-horizontal" method="post" action="../controller/user_home.php">
                
                <div class="form-group">  
                  <label> Username: </label>
                  <input type="number" name="username" class="form-control" placeholder="Enter username" autofocus required>
                </div>
                
                <div class="form-group">
                  <label> Password: </label>
                  <input type="password" name="password" class="form-control" placeholder="Enter password" required>
                </div>
                  
                  <input type="submit" value="Sign In" class="btn btn-default">
            </form>
		
          </div>
	</body>
</html>


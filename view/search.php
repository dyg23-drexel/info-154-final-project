<?php session_start(); ?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title> Search </title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href = "../view/css/search.css" rel = "stylesheet">
        <script type="text/javascript" src="../view/js/showHint.js"> </script>
        <script type="text/javascript" src="../view/js/logOut.js"> </script>
    </head>
    
    <body>
      <nav>
        <label> <?php echo $_SESSION['f_name'].' '.$_SESSION['l_name']; ?> </label>
        <button type="button" onclick="logOut();" class="btn btn-default"> Log Out </button>
      </nav>
      <p class="home"> <a href = "../controller/user_home.php"> Home </a> </p>
        
      <div id="all">
        
        <img src="../images/logo.png" alt="roomo logo"> 
       
        <h1> Search Equipment </h1>
            <br>
            <div id="search_equipment_c">
            
              <form>
            
                <input type = "search" name = "keyword" onkeyup="showHint(this.value);"
                       onmouseover="showInfo(this);" onmouseout="hideInfo(this);" autofocus>
                
              </form>
            </div>
         
        <script type="text/javascript" src="../view/js/show_info.js"> </script>
      </div>
        
      <div id="search_results"> </div>
      
    </body>
</html>

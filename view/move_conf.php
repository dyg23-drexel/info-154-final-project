<?php
   session_start();

   require("../model/database.php");
   require("../model/move_db.php");
  
   $equip_selected_id = '';
   
   $comp = filter_input(INPUT_POST, 'comp');
   $proj = filter_input(INPUT_POST, 'proj');
   $dcam = filter_input(INPUT_POST, 'dcam');
   
   $single_equip_selected = false;
   
   if($comp != 'blank' AND $proj == 'blank' AND $dcam == 'blank')
     {
       $single_equip_selected = true;
       $equip_selected_id = $comp;  
     }
     
   else if($comp == 'blank' AND $proj != 'blank' AND $dcam == 'blank')
     {
       $single_equip_selected = true;
       $equip_selected_id = $proj;
     }
     
   else if($comp == 'blank' AND $proj == 'blank' AND $dcam != 'blank')
     {
       $single_equip_selected = true;
       $equip_selected_id = $dcam;
     }
     
   
   $room_selected_id = '';
     
   $LH = filter_input(INPUT_POST, 'LH');
   $TEC = filter_input(INPUT_POST, 'TEC'); 
   $SB = filter_input(INPUT_POST, 'SB'); 
   $EC = filter_input(INPUT_POST, 'EC'); 
   $BR = filter_input(INPUT_POST, 'BR');
   
   $single_location_selected = false;
   
   
   if($LH != 'blank' AND $TEC == 'blank' AND $SB =='blank' AND $EC == 'blank' AND $BR == 'blank')
     {
       $single_location_selected = true;
       $room_selected_id = $LH;
     }
    
   else if($LH == 'blank' AND $TEC != 'blank' AND $SB =='blank' AND $EC == 'blank' AND $BR == 'blank')
     {
       $single_location_selected = true;
       $room_selected_id = $TEC;
     } 
     
   else if($LH == 'blank' AND $TEC == 'blank' AND $SB !='blank' AND $EC == 'blank' AND $BR == 'blank')
     {
       $single_location_selected = true;
       $room_selected_id = $SB;
     }
   
   else if($LH == 'blank' AND $TEC == 'blank' AND $SB =='blank' AND $EC != 'blank' AND $BR == 'blank')
     {
       $single_location_selected = true;
       $room_selected_id = $EC;
     }
     
   else if($LH == 'blank' AND $TEC == 'blank' AND $SB =='blank' AND $EC == 'blank' AND $BR != 'blank')
     {
       $single_location_selected = true;
       $room_selected_id = $BR;
     }

        
            if($single_equip_selected AND $single_location_selected)
              {
                moveEquip($equip_selected_id, $room_selected_id, $_SESSION['username']);
                
                echo 'Equipment '.$equip_selected_id.' has been successfully moved to room '.$room_selected_id;
              }
     
            else
              {
                echo 'Sorry, you have to select one equipment and one location';
              }
    
?>
    
   

<!DOCTYPE html>

<?php

require('../model/database.php');
require('../model/search_db.php');

$keyword = filter_input(INPUT_POST, 'keyword');

?>


<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href = "../view/css/search_results.css" rel = "stylesheet">
    </head>
    <body>
        
        <div id="results">
        
          <?php 
                
                 if ($keyword == NULL)
                   {
                     echo '<img src = "../images/2.jpg" alt = "sorry"/>';
                     echo '<p> <br/> </p>';
                     echo '<span> Sorry, you need to enter a keyword! </span>';
                     exit();
                   }
                   
                   
                 $results = searchEquip($keyword);
                 
                 if ($results == NULL)
                   {
                     echo '<img src = "../images/2.jpg" alt = "sorry"/>';
                     echo '<p> <br/> </p>';
                     echo '<span> Your search returned no result </span>';
                   }
                  
                 else
                   {

            ?>
        
        <table class="table table-hover table-bordered table-condensed">
            <tr>
                <th> Equip ID </th>
                <th> Equip Brand </th>
                <th> Equip Type </th>
                <th> Equip SN </th>
                <th> Equip Move Date </th>
                <th> Carrier Last Name </th>
                <th> Room ID </th>
                <th> Room SIGNAL </th>
                <th> Building Name </th>
            </tr>

                <?php 
                    
                      foreach($results as $result)
                        { 
                            echo '<tr>';
                         
                            for($i = 0; $i < count($result)/2; $i++)
                               {echo '<td>'.$result[$i].'</td>';}
                            
                            echo '</tr>';
                        }
                   } 
                    
                ?>
           
        </table>
       
      </div>
        
    </body>
</html>

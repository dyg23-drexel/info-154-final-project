// Specify the user info for the textbox
var search_equipment_info = 'Search Equipment by ID, Brand, Type, Serial Number'
                             + ' Move Date, Carrier Last Name, Room ID, Room Signal '
                             + 'or Building Name';

/* 
  Create an array for the info of each search criterion 
  Each criterion is represented by an object which has:
   * A name
   * The info related to it
   * The id of the paragraph in which the info will be displayed
   * The id of the div element in which all the contents related to the
     search criterion are located
*/
var fields_info = [
{name:'keyword',info:search_equipment_info, p_id:'search_equipment_p', d_id:'search_equipment_c'}
];


function showInfo(textbox)
  {
    // This function displays the info of a textbox
    
    // Search in the array,
    for(var i = 0; i < fields_info.length; i++)
      {
        // If the name of the textbox is found
        if (textbox.name === fields_info[i].name)
          {
            // Create a paragraph element
            var p = document.createElement('P');
            
            // Set the id of the paragraph
            p.id = fields_info[i].p_id;
            
            // Set the criterion info as the node of the paragraph
            var t = document.createTextNode(fields_info[i].info);
            
            // Append the node to the paragraph
            p.appendChild(t);
            
            // Append the paragraph to the div of the criterion
            document.getElementById(fields_info[i].d_id).appendChild(p);
          }
      }
  }

function hideInfo(textbox)
  {
    // This function hides the info of a textbox
    
    // Loop into the array of criteria
    for(var i = 0; i < fields_info.length; i++)
      {
        // If the name of the textbox matches the name of a criterion,
        if (textbox.name === fields_info[i].name)
          {
            // Delete the info located in the paragraph of the textbox
            document.getElementById(fields_info[i].p_id).remove();
          }
      }
  }

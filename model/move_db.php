<?php

function moveEquip($eq_id, $target_location, $carrier_lname)
  {
    global $db;
    
    $query = 'UPDATE equipment e SET e.EQ_LOCATION = "'.$target_location.'" '
            . 'WHERE e.EQ_ID = "'.$eq_id.'"';
    
    $statement = $db->prepare($query);
    
    $statement->execute();
    
    $statement->closeCursor();
    
    setEquipMoveDate($eq_id);
    setEquipCarrier($carrier_lname, $eq_id);
  }
  
  
function setEquipMoveDate($eq_id)
  {
    global $db;
    
    $query = 'UPDATE equipment e SET e.EQ_MOVE_DATE = SYSDATE() '
            . 'WHERE e.EQ_ID = "'.$eq_id.'"';
    
    $statement = $db->prepare($query);
    
    $statement->execute();
    
    $statement->closeCursor();
  }
  
  
 function setEquipCarrier($carrier_lname, $eq_id)
  {
    global $db;
    
    $query = 'UPDATE equipment e SET e.EQ_CARRIER = "'.$carrier_lname
            . '" WHERE e.EQ_ID = "'.$eq_id.'"';
    
    $statement = $db->prepare($query);
    
    $statement->execute();
    
    $statement->closeCursor();
  }


<?php

function viewAllEquip()
  {
    global $db;
    
    $query = 'SELECT e.EQ_ID, e.EQ_BRAND, e.EQ_TYPE, e.EQ_SN, e.EQ_MOVE_DATE, 
              c.CARRIER_LNAME, r.ROOM_ID, r.ROOM_SIGNAL, b.BLDG_NAME FROM equipment e, carrier c, 
              room r, building b WHERE (c.CARRIER_ID = e.EQ_CARRIER 
              AND e.EQ_LOCATION = r.ROOM_ID AND r.ROOM_LOCATION = b.BLDG_ID)
              ORDER BY e.EQ_ID ASC';
    
    $statement = $db->prepare($query);
    
    $statement->execute();
    
    $results = $statement->fetchAll();
    
    $statement->closeCursor();
    
    return $results;
  }
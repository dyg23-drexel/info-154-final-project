<?php

$dsn = 'mysql:host=localhost;dbname=roomo';
$username = 'mgs_tester';
$password = '';
  
try
  {
    $db = new PDO($dsn, $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } 

catch (PDOException $e) 
  {
    echo 'The database generated the following error: '.$e->getMessage();
    exit();
  }



